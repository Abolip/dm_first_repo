PS C:\Users\Z004ZFST\dm_first_repo> git log
commit 3c3eeb3f1806e3fbe6f542e3e5b9789b92014aa5 (HEAD -> master, origin/master, origin/HEAD)
Merge: 5e93c6e 7f630c3
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Sat Jun 8 14:36:56 2024 +0530

    Updated readme.md in Abolip branch

commit 5e93c6e51827075fe3e1d95adfd17bc1e2afa8c5
Merge: 6a123a5 a70ef53
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Sat Jun 8 14:28:03 2024 +0530

    Conflict resolved

commit 6a123a5cdf5d748e9a1ec1622b12f5403f74e172
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Sat Jun 8 12:39:39 2024 +0530

    Add .gitignore file to main branch

commit a70ef533c35949ea86e9176af7cffcb09ecc66ea (origin/dev)
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Sat Jun 8 06:55:47 2024 +0000

    Delete .gitignore

commit 7f630c3c6e6750d04384246de5ee6fcd9dbe0125
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Sat Jun 8 12:23:08 2024 +0530

    Add Readme file in username branch

commit 2f504675f244520cd7cad7fa9654560c0006c45c (z004zfst-new_branch)
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Sat Jun 8 06:36:04 2024 +0000

    Delete README.md

commit 8ce4c225f9447a754a03a59203d52e6dee3b54bb
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Sat Jun 8 12:05:08 2024 +0530

    Add Task1 folder with README.md

commit 39e3bbe1f9f7a03e92d38ad3bbbb94f2d5a2fe50
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Thu Jun 6 09:59:34 2024 +0000

    Update git_commands.md

commit d3005b56b23defda74d787b3a8a35607b01091c7
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Thu Jun 6 12:57:54 2024 +0530

    commit for gitgnore

commit cc0605d966bdc390f2a94baf78af27bba2a33046
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Wed Jun 5 19:41:47 2024 +0000

    added updated file

commit df4343bdc8ea6a86d13906205783d20d5c020be8
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Wed Jun 5 19:34:09 2024 +0000

    Delete .greet

commit 0903c622006fd604d79f290bcfa67f597db5e696
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Wed Jun 5 19:33:54 2024 +0000

    Delete ,gitignore

commit 453ba0bd9453ffd9ea578385653a7a403189436e
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Wed Jun 5 19:33:19 2024 +0000

    Delete .gitignore

commit ef533e2e03e4504a030e1aff73f5325f3404f944
Merge: 118b1f8 ed4c476
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Wed Jun 5 19:32:36 2024 +0000

    Merge branch 'dev' into 'master'

    todays last commit

    See merge request Abolip/dm_first_repo!2

commit ed4c4763a9286f0b1eb461e9a2ecefddc0ca9ea1
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Thu Jun 6 01:00:26 2024 +0530

    todays last commit

commit 118b1f89f418749a11ba8dde798d0b5ed581b8bf
Merge: e8ca859 a5462dd
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Wed Jun 5 17:45:21 2024 +0000

    Merge branch '%USERNAME-new_feature' into 'master'

    My third commit

    See merge request Abolip/dm_first_repo!1

commit a5462dd6a2b22bb63deacf52d141a21cd76c7727
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Wed Jun 5 22:48:11 2024 +0530

    My second commit

commit e8ca85941679b70ef62a4ff4f85bce73bba109b6
Author: Abolip <aboliapatil2002@gmail.com>
Date:   Wed Jun 5 22:24:41 2024 +0530

    My First Commit
(END)